		document.addEventListener('DOMContentLoaded', ()=>{
			// const button_showModal = document.querySelector('button.show_input_modal');
			// button_showModal.addEventListener('click', showModalInputEmail);

			const price_link = document.querySelector('.price_link');
			price_link.addEventListener('click', showModalInputEmail);
			
			const button_sendRequest = document.querySelector('button.send_request');
			button_sendRequest.addEventListener('click', ()=>{
				// check email
				const xhr = new XMLHttpRequest();
				const data = new FormData();
				const inputEmail_value = document.querySelector('input.modal_inputEmail_input').value;
				const inputEmail_value_encoded = encodeString(inputEmail_value);
				// console.log(inputEmail_value_encoded);
				const inputEmail_test = testEmail(inputEmail_value);
				const input_container = document.querySelector('.input_container');
				const success_container = document.querySelector('.success_container');
				const success_message = document.querySelector('.success_message');
				if (!inputEmail_test) {
					// alert('Введите другой адрес электронной почты');
					// xhr.abort();
					return;
				}
				// console.log(data);
				data.append('mail', inputEmail_value_encoded);
				xhr.addEventListener('load', ()=>{
					if (xhr.status == 200 && xhr.readyState == 4) {
						// console.log(xhr.response);
						// xhr.response == '1' ? alert('email sent!') : alert('email failed');
						let response = xhr.response;
						const button = document.querySelector('.send_request');
						if (response == 1) {
							//'Email has sent successfully.';
							input_container.classList.add('hidden');
							success_message.innerHTML += inputEmail_value;
							success_container.classList.remove('hidden');
						} else {
							button.setAttribute('disabled', 'true');
							if (response == '-1') {
								button.textContent = 'Неправильный формат';
							}
							if (response == 0 ) {
								// 'Email sending failed.'
								button.textContent = 'Возникла ошибка при отправке';
							}
						}
					}
				});
				xhr.open('POST', '/php/mail_test.php');
				xhr.send(data);
			});

			const button_ok = document.querySelector('.button_ok');
			const button_close = document.querySelector('.modal_inputEmail_closeButton');
			const modal_inputEmail = document.querySelector('.modal_inputEmail');
			button_ok.addEventListener('click', () => {
				modal_inputEmail.classList.add('hidden');
			})
			button_close.addEventListener('click', () => {
				modal_inputEmail.classList.add('hidden');
			})

			document.querySelector('.modal_inputEmail_input').addEventListener('keyup', (e)=>{
				const input = document.querySelector('.modal_inputEmail_input');
				const button = document.querySelector('.send_request');
				if (!testEmail(input.value) && input.value.length != 0) {
					button.textContent = 'Неправильный формат';
					button.setAttribute('disabled', 'true');
				}
				else {
					button.textContent = 'Отправить запрос';
					button.removeAttribute('disabled');
				}
			});
		})


		var showModalInputEmail = function() {
			document.querySelector('.modal_inputEmail').classList.toggle('hidden');
			document.querySelector('.modal_inputEmail_input').focus();
		}

		var testEmail = function(input) {
			const email_regex = /^[a-z0-9\_\-\.]{1,61}\@[a-z0-9\_\-]{1,16}\.[a-z]{2,3}$/i;
			return email_regex.test(input);
		}

		var encodeString = function (str) {
			// Firstly, escape the string using encodeURIComponent to get the UTF-8 encoding of the characters, 
			// Secondly, we convert the percent encodings into raw bytes, and add it to btoa() function.
			utf8Bytes = encodeURIComponent(str).replace(/%([0-9A-F]{2})/g, function (match, p1) {
				return String.fromCharCode('0x' + p1);
			});

			return btoa(utf8Bytes);
		}

		var decodeString = function (str) {
			return atob(str);
		}