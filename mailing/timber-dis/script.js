document.addEventListener("DOMContentLoaded", () => {
  document.custom = { ...document.custom, controls: getControls() };
  reloadIframe();
  document.custom.controls.select.addEventListener("change", reloadIframe);
  console.log(document.custom);
});

const getControls = () => ({
  select: document.querySelector("#mailing-variant"),
  iframe: document.querySelector(".preview-iframe"),
  title: document.querySelector(".preview-title"),
});

const reloadIframe = () => {
  const selectData = document.custom.controls.select.selectedOptions[0].dataset;
  const optionUrl = selectData.url
  const optionTitle = selectData.title
  document.custom.controls.iframe.setAttribute("src", optionUrl);
  document.custom.controls.title.textContent = optionTitle;
};