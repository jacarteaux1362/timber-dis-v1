<?php
	$to = "";
	$to = htmlspecialchars($_POST['mail']);
	$to = base64_decode($to);
	if (!filter_var($to, FILTER_VALIDATE_EMAIL)) {
	  echo '-1'; //"Invalid email format";
	} else {


	$conn = db_connect();

	$to_escaped = $conn->real_escape_string($to);
	$request_emails = "SELECT email FROM price_request_emails WHERE email='$to_escaped'";
	// $request_emails = $conn->real_escape_string($request_emails);
	$result_emails = $conn->query($request_emails);
	date_default_timezone_set("Europe/Moscow");
	$date =  date("d.m.Y H:i");
	if ($result_emails->num_rows > 0) {
		$request_update = "UPDATE `price_request_emails` SET `date`='$date', `number_of_requests`=(case WHEN `email`='$to_escaped' then `number_of_requests`+1  end) WHERE `email`='$to_escaped'";
	} else {
		$request_update = "INSERT INTO `price_request_emails`(`date`, `email`, `number_of_requests`) VALUES ('$date', '$to_escaped', '1')";
	}
	// $request_update = $conn->real_escape_string($request_update);
	$db_update_result = '';
	$result_update = $conn->query($request_update);
	if ($result_update) {
		$db_update_result = "База данных обновлена.";
	} else {
		$db_update_result = "База данных не обновлена.\n". $result_update;		
	}
	$result_emails->free_result();
	$conn->close();


		// echo $to;
		$from = 'info@timber-dis.ru'; 
		$fromName = 'Тимбэ Дистрибуция';
		$htmlContent = '<table width="500px" border="0" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse"><tr><td><table width="500px" border="0" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse"><tr><td width="175" align="center" valign="middle"><span style="font-family:Verdana,Geneva,sans-serif;font-size:12px;line-height:30px;color:#575756;padding:0;margin:0;margin-top:25px;margin-bottom:15px">+7 977 270 71 07<br>info@timber-dis.ru<br>https://timber-dis.ru</span></td><td><img src="https://timber-dis.ru/pic/for_mail/cover1_e.jpg"></td></tr></table></td></tr><tr><td><table width="500px" border="0" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse"><tr valign="middle" align="center"><td colspan="2"><p style="font-family:Verdana,Geneva,sans-serif;font-size:12px;padding:0;margin-left:55px;margin-right:55px;margin-bottom:15px;margin-top:15px;line-height:25px">Мы, ООО "Тимбэ Дистрибуция", предлагаем широкий ассортимент продукции из алтайской берёзы: расчёски, массажёры, пилки, совочки, лопатки.<br>Отгрузка со склада в Москве.<br>Натуральное сырьё, высококлассные итальянские и немецкие станки делают нашу продукцию вне конкуренции.<br>Ознакомиться с ассортиментом Вы можете на сайте<br><a href="https://timber-dis.ru/" style="color:#006934">timber-dis.ru</a>.</p></td></tr></table></td></tr><tr><td><table width="500px" border="0" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse;margin-top:35px"><tr><td width="125px" align="center"><img src="https://timber-dis.ru/pic/for_mail/border_left__short.jpg"></td><td width="125px" valign="middle" align="center"><h1 style="font-family:Verdana,Geneva,sans-serif;font-size:14px;padding:0;margin:0;margin-bottom:7px;margin-top:7px;line-height:20px;color:#006934;font-weight:700">Условия сотрудничества</h1></td><td width="125px" align="center"><img src="https://timber-dis.ru/pic/for_mail/border_right__short.jpg"></td></tr></table></td></tr><tr><td><table width="500px" border="0" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse"><tr><td valign="middle" align="center"><ul style="font-family:Verdana,Geneva,sans-serif;font-size:12px;margin:0;margin-bottom:0;line-height:18px;line-height:35px;list-style-type:none;padding-left:0;text-align:center"><li><img src="https://timber-dis.ru/pic/for_mail/leaf_copy.jpg" style="position:relative;top:3px"> Мы не ограничиваем минимальную партию заказа</li><li><img src="https://timber-dis.ru/pic/for_mail/leaf_copy.jpg" style="position:relative;top:3px"> Отгружаем по факту оплаты</li><li><img src="https://timber-dis.ru/pic/for_mail/leaf_copy.jpg" style="position:relative;top:3px"> Доставка до терминала Вашей транспортной компании за наш счёт</li></ul></td></tr></table></td></tr><tr><td><table width="500px" border="0" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse;margin-top:35px"><tr><td width="150px" align="center"><img src="https://timber-dis.ru/pic/for_mail/border_left.jpg"></td><td width="200px" valign="middle" align="center"><h1 style="font-family:Verdana,Geneva,sans-serif;font-size:14px;padding:0;margin:0;margin-bottom:7px;margin-top:7px;line-height:18px;color:#006934;font-weight:700"><a href="https://timber-dis.ru/production/combs.html" style="font-family:Verdana,Geneva,sans-serif;font-size:14px;padding:0;margin:0;margin-bottom:7px;margin-top:7px;line-height:18px;color:#006934;font-weight:700">Расчёски</a></h1></td><td width="150px" align="center"><img src="https://timber-dis.ru/pic/for_mail/border_right.jpg"></td></tr></table></td></tr><tr><td><table width="500px" border="0" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse"><tr><td><img src="https://timber-dis.ru/pic/for_mail/rd1104.jpg" alt="Расчёска РД1104" title="Расчёска РД1104"></td><td valign="middle" align="center"><span style="font-family:Verdana,Geneva,sans-serif;font-size:12px;padding:0;margin:0;margin-bottom:15px;line-height:18px">Большой выбор расчёсок и гребней разных размеров, модификаций и форм.</span></td></tr><tr><td valign="middle" align="center"><span style="font-family:Verdana,Geneva,sans-serif;font-size:12px;padding:0;margin:0;margin-bottom:15px;line-height:18px">Расчёски и гребни поставляются в индивидуальной упаковке с еврослотом.</span></td><td><img src="https://timber-dis.ru/pic/for_mail/rd3101.jpg" alt="Расчёска РД3101" title="Расчёска РД3101"></td></tr><tr><td valign="middle" align="center"><span style="font-family:Verdana,Geneva,sans-serif;font-size:12px;padding:0;margin:0;margin-bottom:15px;line-height:18px">Удобны в использовании. Полезны для волос и кожи головы. Популярны в качестве нанесения косметических и лечебных средств.</span></td><td><img src="https://timber-dis.ru/pic/for_mail/rd5101.jpg" alt="Расчёска РД5101" title="Расчёска РД5101"></td></tr></table></td></tr><tr><td><table width="500px" border="0" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse;margin-top:35px"><tr><td width="200px" align="center"><img src="https://timber-dis.ru/pic/for_mail/border_left.jpg"></td><td width="100px" valign="middle" align="center"><h1 style="font-family:Verdana,Geneva,sans-serif;font-size:14px;padding:0;margin:0;margin-bottom:7px;margin-top:7px;line-height:18px;color:#006934;font-weight:700">Массажёры</h1></td><td width="200px" align="center"><img src="https://timber-dis.ru/pic/for_mail/border_right.jpg"></td></tr></table></td></tr><tr><td><table width="500px" border="0" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse"><tr><td><img src="https://timber-dis.ru/pic/for_mail/ma3224.jpg" alt="Массажёр &#34;Ленточный&#34; МА3224" title="Массажёр &#34;Ленточный&#34; МА3224"></td><td valign="middle" align="center"><span style="font-family:Verdana,Geneva,sans-serif;font-size:12px;padding:0;margin:0;margin-bottom:15px;line-height:18px">Массажёры широкого спектра применения: <a href="https://timber-dis.ru/production/massagers/back.html" style="color:#006934">для спины</a>, <a href="https://timber-dis.ru/production/massagers/foot.html" style="color:#006934">стоп</a> и <a href="https://timber-dis.ru/production/massagers/hand.html" style="color:#006934">рук</a>.</span></td></tr><tr><td valign="middle" align="center"><span style="font-family:Verdana,Geneva,sans-serif;font-size:12px;padding:0;margin:0;margin-bottom:15px;line-height:18px">Зубчатые вращающиеся ролики благотворно влияют на нервные окончания.</span></td><td><img src="https://timber-dis.ru/pic/for_mail/ma4213.jpg" alt="Массажёр &#34;Счёты&#34; МА4213" title="Массажёр &#34;Счёты&#34; МА4213"></td></tr><tr><td><img src="https://timber-dis.ru/pic/for_mail/ma8401.jpg" alt="Массажёр &#34;Качалка&#34; МА8401" title="Массажёр &#34;Качалка&#34; МА8401"></td><td valign="middle" align="center"><span style="font-family:Verdana,Geneva,sans-serif;font-size:12px;padding:0;margin:0;margin-bottom:15px;line-height:18px">При регулярном использовании оказывают тонизирующее воздействие на организм.</span></td></tr><tr><td valign="middle" align="center"><span style="font-family:Verdana,Geneva,sans-serif;font-size:12px;padding:0;margin:0;margin-bottom:15px;line-height:18px">Эргономичный дизайн и упаковка сделают полезный подарок приятным.</span></td><td><img src="https://timber-dis.ru/pic/for_mail/ma4301.jpg" alt="Массажёр МА4301" title="Массажёр МА4301"></td></tr></table></td></tr><tr><td><table width="500px" border="0" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse;margin-top:35px"><tr><td width="200px" align="center"><img src="https://timber-dis.ru/pic/for_mail/border_left.jpg"></td><td width="100px" valign="middle" align="center"><h1 style="font-family:Verdana,Geneva,sans-serif;font-size:14px;padding:0;margin:0;margin-bottom:7px;margin-top:7px;line-height:18px;color:#006934;font-weight:700"><a href="https://timber-dis.ru/production/foot_files.html" style="font-family:Verdana,Geneva,sans-serif;font-size:14px;padding:0;margin:0;margin-bottom:7px;margin-top:7px;line-height:18px;color:#006934;font-weight:700">Пилки</a></h1></td><td width="200px" align="center"><img src="https://timber-dis.ru/pic/for_mail/border_right.jpg"></td></tr></table></td></tr><tr><td><table width="500px" border="0" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse"><tr><td><img src="https://timber-dis.ru/pic/for_mail/pa1105.jpg" alt="Пилка абразивная 1105 большая" title="Пилка абразивная 1105 большая"></td><td valign="middle" align="center"><span style="font-family:Verdana,Geneva,sans-serif;font-size:12px;padding:0;margin:0;margin-bottom:15px;line-height:18px">Пилки для стоп разного размера и формы из алтайской берёзы.</span></td></tr><tr><td valign="middle" align="center"><span style="font-family:Verdana,Geneva,sans-serif;font-size:12px;padding:0;margin:0;margin-bottom:15px;line-height:18px">Двусторонние тёрки различной абразивности.</span></td><td><img src="https://timber-dis.ru/pic/for_mail/pa1106.jpg" alt="Пилка абразивная 1106 фасонная" title="Пилка абразивная 1106 фасонная"></td></tr><tr><td><img src="https://timber-dis.ru/pic/for_mail/pa1204.jpg" alt="Пилка абразивная 1204 малая" title="Пилка абразивная 1204 малая"></td><td valign="middle" align="center"><span style="font-family:Verdana,Geneva,sans-serif;font-size:12px;padding:0;margin:0;margin-bottom:15px;line-height:18px">При регулярном использовании оказывают тонизирующее воздействие на организм.</span></td></tr></table></td></tr><tr><td><table width="500px" border="0" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse;text-align:center;margin-top:35px"><tr><td colspan="2"><span style="font-family:Verdana,Geneva,sans-serif;font-size:10px;padding:0;margin-left:0;margin-right:0;margin-bottom:0;margin-top:0;line-height:18px;color:#a5a5a5">г. Москва, ООО "Тимбэ Дистрибуция" 2021</span></td><td><span style="font-family:Verdana,Geneva,sans-serif;font-size:10px;padding:0;margin-left:0;margin-right:0;margin-bottom:0;margin-top:0;line-height:18px;color:#a5a5a5">ИНН: 7716874820</span></td></tr><tr><td><span style="font-family:Verdana,Geneva,sans-serif;font-size:10px;padding:0;margin-left:0;margin-right:0;margin-bottom:0;margin-top:0;line-height:18px;color:#a5a5a5">ОКПО: 19993280</span></td><td><span style="font-family:Verdana,Geneva,sans-serif;font-size:10px;padding:0;margin-left:0;margin-right:0;margin-bottom:0;line-height:18px;color:#a5a5a5">КПП: 771501001</span></td><td><span style="font-family:Verdana,Geneva,sans-serif;font-size:10px;padding:0;margin-left:0;margin-right:0;margin-bottom:0;line-height:18px;color:#a5a5a5">ОГРН: 5177746127004</span></td></tr></table></td></tr><tr><td></td></tr></table>';
		$headers = "From: $fromName" . " <" . $from . ">"; 
		// Boundary
		$semi_rand = md5(time());
		$mime_boundary = "==Multipart_Boundary_x{$semi_rand}x";
		// Headers for attachment  
		$headers .= "\nMIME-Version: 1.0\n" . "Content-Type: multipart/mixed;\n" . " boundary=\"{$mime_boundary}\"";
		// Multipart boundary  
		$message = "--{$mime_boundary}\n" . "Content-Type: text/html; charset=\"UTF-8\"\n" .
		"Content-Transfer-Encoding: 7bit\n\n" . $htmlContent . "\n\n";
		// Preparing attachment 
		$file = 'Order List. Timber Distribution.xlsx';
		if (!empty($file) > 0) {
			if (is_file($file)) {
				$message .= "--{$mime_boundary}\n";
				$fp =    @fopen($file, "rb");
				$data =  @fread($fp, filesize($file));

				@fclose($fp);
				$data = chunk_split(base64_encode($data));
				$message .= "Content-Type: Content-Type: application/octet-stream; name=\"" . basename($file) . "\"\n" .
					"Content-Description: " . basename($file) . "\n" .
					"Content-Disposition: attachment;\n" . " filename=\"" . basename($file) . "\"; size=" . filesize($file) . ";\n" .
					"Content-Transfer-Encoding: base64\n\n" . $data . "\n\n";
			}
		}
		$message .= "--{$mime_boundary}--";
		$returnpath = "-f" . $from;

		$subject = "Актуальный прайс-лист Тимбэ Дистрибуция"; 
		 
		// $message = "Здравствуйте!\nВ ответ на запрос прайс-листа, оптправляем актуальный прайс-лист во вложении.\nСпасибо!"; 
		$message_dis = "Прайс отправлен на почту $to\n$db_update_result"; 
		 
		// // Additional headers 
		// $headers = 'From: '.$fromName.'<'.$from.'>'; 

		// // Send email 
		// if(mail($to, $subject, $message, $headers)){
		if($mail = @mail($to, $subject, $message, $headers, $returnpath)){
		echo '1' ; //'Email has sent successfully.'; 
			mail($from, 'Отправлен прайс на почту '.$to, $message_dis, 'From: ' . $fromName . '<' . $from . '>');
		} else{ 
			echo '0'; // 'Email sending failed.'; 
		}
	}

function db_connect()
{
	$serveranme = 'localhost';
	$db_name = 'ce17531_db1';
	$user = 'ce17531_db1';
	$passowrd = '8?Th2HNU!Y:u<?Z?HeO';
	$conn = mysqli_connect($serveranme, $user, $passowrd, $db_name);
	

	if (!$conn) {
		die('Connection failed: ' . mysqli_connect_error());
	}

	// printf("Initial character set: %s\n", $conn->character_set_name());
	/* change character set to utf8 */
	if (!$conn->set_charset("utf8")) {
		printf("Error loading character set utf8: %s\n", $conn->error);
		exit();
	}
	// else {
	// 	printf("Current character set: %s\n", $conn->character_set_name());
	// }

	return $conn;
}

	function db_close($db_connection)
	{
		$db_connection->close();
	}
